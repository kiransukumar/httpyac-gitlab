let text = "<@ request.json >> realrequest.json";
//const fileImport = /^\s*>>(?<force>!)?\s+(?<fileName>.+)\s*$/u.exec(text);
const fileImport = /^<(?:(?<injectVariables>@)(?<encoding>\w+)?)?\s+(?<fileName>.+?)\s+>>\s+(?<outputFile>.+?)\s*$/u.exec(text);

console.log(fileImport.length); // Outputs: array with matched groups